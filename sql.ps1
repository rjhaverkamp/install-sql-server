function CreateDirectory ($name) {
    New-Item $name -type directory
}

function DownloadFiles ($file, $location) {
    # Actualy download ISO and Config file
    $wc = New-Object System.Net.WebClient
    $wc.DownloadFile($file, $location)
}

# files
$isofile = "sql.iso" #  filename for SQL ISO file
$configfile = "ConfigurationFile.ini"

# directories
$storageDir = "C:\Downloads"
$logDir = "E:\LOG"
$dbDir = "E:\DB"

# locations to store downloaded files
$sqliso = "$storageDir\$isofile"
$sqlconfig = "$storageDir\$configfile"

# websites to download files from
$sqlconfigdownload = "http://172.16.0.41:8080/$configfile" # configfile location
$sqlURL = "http://172.16.0.41:8080/$isofile"# SQL ISO location

$password # SQL user password

# Create the directories
CreateDirectory -name $storageDir
CreateDirectory -name $logDir
CreateDirectory -name $dbDir

# Download files
DownloadFiles -file $sqlURL -location $sqliso
DownloadFiles -file $sqlconfigdownload -location $sqlconfig

# Mount ISO file and get Drive to which it is mapped.
$mountResult = Mount-DiskImage $sqliso -PassThru
$setuppath = ($mountResult | Get-Volume).DriveLetter + ":\" + "setup.exe"


# prepare SQL server installation
$InstallLog = "$logDir\InstallLog.log"
$argumentlist = "/configurationfile=$sqlconfig /action=install /iacceptsqlserverlicenseterms /sapwd=""$password"" /sqlsysadminaccounts=$admin /Q"
Get-WindowsFeature NET-Framework-Core | install-windowsfeature

Start-Process $setuppath -Argumentlist $argumentlist -Wait -RedirectStandardOutput $InstallLog
Dismount-DiskImage -ImagePath $sqliso
Remove-Item $storageDir -Recurse -Force