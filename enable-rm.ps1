# got this code from David Ford(https://social.technet.microsoft.com/profile/david%20ford%20-%20davidfordaus/) found it at: 
# https://technet.microsoft.com/nl-nl/library/dd206997(v=sql.105).aspx
Import-Module "sqlps"
$smo = 'Microsoft.SqlServer.Management.Smo.'
$wmi = new-object ($smo + 'Wmi.ManagedComputer').

# List the object properties, including the instance names.
$Wmi

# Enable the TCP protocol on the default instance.
$uri = "ManagedComputer[@Name='" + (get-item env:\computername).Value + "']/ServerInstance[@Name='MSSQLSERVER']/ServerProtocol[@Name='Tcp']"
$Tcp = $wmi.GetSmoObject($uri)
$Tcp.IsEnabled = $true
$Tcp.Alter()
$Tcp

$tcp.ipaddresses[1].ipaddressproperties
foreach ($tcpAddress in $Tcp.ipaddresses) {
    foreach ($property in $tcpAddress.ipaddressproperties) {
        if ($property.Name -eq "Enabled") {
            
            $property.Value=$true
            $Tcp.Alter
        
    }


        if ($property.Name -eq "Port") {
            $property.Value=1433
            $Tcp.Alter
        }
    }
}

$Tcp

Restart-Service 'MSSQL$SQLSERVER' -Force